#!/bin/bash
cd /tmp
git clone https://github.com/soimort/translate-shell
cd translate-shell/
make install
cd ..
rm -rf translate-shell
